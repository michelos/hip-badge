# Hacking in Parallel Badge
Hacking In Parallel (HiP) was a conference that happened in Berlin in December, 2022. All attendees received an electronic badge with many RGB LED Lights, USB, Battery Charging Control, NFC Radio, TVOC (Total Volatile Organic Compounds) air quality sensor, and a number of other way cool features.

## Comms

And join our matrix discussion channel: https://matrix.to/#/#hipbadge:matrix.org

## Docs
[Badge general description](documents/README.md)

[Firmware](firmware/README.md)

[Graphics](graphics/README.md)

[Hardware](hardware/README.md)

[Mechanic](mechanic/README.md)

[Software](software/README.md)
